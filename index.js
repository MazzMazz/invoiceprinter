var fs = require('fs');
var pdf = require('html-pdf');
var https = require('https');
var http = require('http');
var config = require('cloud-env')

var server_port = config.PORT || 8080
var server_ip_address = config.IP || '127.0.0.1'

    http.createServer(function (req, response) {
        try {
            /*response.writeHead(200, {'Content-Type': 'application/pdf'});

            // console.log(req.getParameter('invoice'));
            // TODO: Get host and path
            var options = {
                host: 'dev.vereinsplaner.at',
                path: '/app/print/invoice/2af79845851937e1fb0687f63e409df361898255'
            };

            var request = https.request(options, function (res) {
                var data = '';
                res.on('data', function (chunk) {
                    data += chunk;
                });
                res.on('end', function () {
                    pdf.create(data.toString(), {
                        format: 'A4',
                        "header": {
                            "height": "0mm",
                            "contents": ''
                        },
                        "zoomFactor": 0.7
                    }).toBuffer(function (err, buffer) {
                        if (err) {
                            return console.log(err);
                        }
                        console.log('end');
                        response.write(buffer);
                        response.end();
                    });

                });
            });

            request.on('error', function (e) {
                console.log(e.message);
            });

            request.end();*/
            console.log((new Date()) + ' Received request for ' + request.url);
            response.writeHead(200, {'Content-Type': 'text/plain'});
            response.write("Welcome to Node.js on OpenShift!\n\n");
            response.end("Thanks for visiting us! \n");
        } catch (e) {

        }
    }).listen(server_port, server_ip_address, function() {

        console.log('listening on port ' + server_port);
        console.log('listening on ip ' + server_ip_address);
    });


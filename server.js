//  OpenShift sample Node application
var express = require('express'),
    app = express(),
    morgan = require('morgan');
var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '400mb'})); // support json encoded bodies
app.use(bodyParser.urlencoded({limit: '400mb', extended: true})); // support encoded bodies

Object.assign = require('object-assign')

app.engine('html', require('ejs').renderFile);
app.use(morgan('combined'))

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
    mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
    mongoURLLabel = "";

if (mongoURL == null && process.env.DATABASE_SERVICE_NAME) {
    var mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase(),
        mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'],
        mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'],
        mongoDatabase = process.env[mongoServiceName + '_DATABASE'],
        mongoPassword = process.env[mongoServiceName + '_PASSWORD']
    mongoUser = process.env[mongoServiceName + '_USER'];

    if (mongoHost && mongoPort && mongoDatabase) {
        mongoURLLabel = mongoURL = 'mongodb://';
        if (mongoUser && mongoPassword) {
            mongoURL += mongoUser + ':' + mongoPassword + '@';
        }
        // Provide UI label that excludes user id and pw
        mongoURLLabel += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
        mongoURL += mongoHost + ':' + mongoPort + '/' + mongoDatabase;

    }
}
var db = null,
    dbDetails = new Object();

var initDb = function (callback) {
    if (mongoURL == null) return;

    var mongodb = require('mongodb');
    if (mongodb == null) return;

    mongodb.connect(mongoURL, function (err, conn) {
        if (err) {
            callback(err);
            return;
        }

        db = conn;
        dbDetails.databaseName = db.databaseName;
        dbDetails.url = mongoURLLabel;
        dbDetails.type = 'MongoDB';

        console.log('Connected to MongoDB at: %s', mongoURL);
    });
};

app.get('/', function (req, res) {
    // try to initialize the db on every request if it's not already
    // initialized.
    if (!db) {
        initDb(function (err) {
        });
    }
    if (db) {
        var col = db.collection('counts');
        // Create a document with request IP and current time of request
        col.insert({ip: req.ip, date: Date.now()});
        col.count(function (err, count) {
            if (err) {
                console.log('Error running count. Message:\n' + err);
            }
            res.render('index.html', {pageCountMessage: count, dbInfo: dbDetails});
        });
    } else {
        res.render('index.html', {pageCountMessage: null});
    }
});

app.get('/pdf', function (req, res) {
    var https = require('https');
    var pdf = require('html-pdf');
    const URL = require('url');

    var reqUrl = decodeURIComponent(req.query.url || '');
    var validator = req.query.validator || '';

    if (validator !== 'vpmazzbp') {
        res.write('validator is invalid');
        res.end();
        return;
    } else if (!reqUrl.length) {
        res.write('Please provide a url');
        res.end();
        return;
    }

    try {
        var urlObject = URL.parse(reqUrl);
        var host = urlObject.hostname;
        var path = urlObject.pathname + urlObject.search;

        var options = {
            host: host,
            path: path
        };

        var request = https.request(options, function (fileRes) {
            var data = '';
            fileRes.on('data', function (chunk) {
                data += chunk;
            });

            var border = req.query.border || {};

            fileRes.on('end', function () {
                pdf.create(data.toString(), {
                    format: 'A4',
                    "header": {
                        "height": "0mm",
                        "contents": ''
                    },
                    "orientation": req.query.orientation || "portrait",
                    "zoomFactor": 1,
                    "border": {
                        "top": border.top || "15mm",
                        "right": border.right || "15mm",
                        "bottom": border.bottom || "15mm",
                        "left": border.left || "15mm"
                    },
                    "footer": {
                        "height": (req.query.footer || '').length ? "28mm" : '0mm',
                        "contents": {
                            default: req.query.footer || ''
                        }
                    },
                    "timeout": '100000'
                }).toBuffer(function (err, buffer) {
                    if (err) {
                        return console.log(err);
                    }
                    res.writeHead(200, {'Content-Type': 'application/pdf'});
                    res.write(buffer);
                    res.end();
                });

            });
        }).on('error', function (e) {
            console.log(e.message);
        });

        request.end();
    } catch (e) {
        console.log(e.message);
        res.write('Something went wrong, sorry!');
        res.end();
    }
});

app.post('/pdf', function (req, res) {

    var pdf = require('html-pdf');

    var validator = req.query.validator || '';
    var data = req.body.html;

    if (validator !== 'vpmazzbp') {
        res.write('validator is invalid');
        res.end();
        return;
    } else if (!data.length) {
        res.write('Please provide a body');
        res.end();
        return;
    }

    try {

        pdf.create(data.toString(), {
            format: 'A4',
            "header": {
                "height": req.body.headerHeight || "0mm",
                "contents": ''
            },
            "orientation": req.query.orientation || "portrait",
            "zoomFactor": 1,
            "border": {
                "top": req.body.borderTop || req.body.border || "15mm",
                "right": req.body.borderRight || req.body.border || "15mm",
                "bottom": req.body.borderBottom || req.body.border || "15mm",
                "left": req.body.borderLeft || req.body.border || "15mm"
            },
            "footer": {
                "height": req.body.footerHeight || '0mm',
                "contents": {
                    default: req.query.footer || ''
                }
            },
            "timeout": '100000'
        }).toBuffer(function (err, buffer) {
            if (err) {
                return console.log(err);
            }
            res.writeHead(200, {'Content-Type': 'application/pdf'});
            res.write(buffer);
            res.end();
        });
    } catch (e) {
        console.log(e.message);
        res.write('Something went wrong, sorry!');
        res.end();
    }
});

app.get('/pagecount', function (req, res) {
    // try to initialize the db on every request if it's not already
    // initialized.
    if (!db) {
        initDb(function (err) {
        });
    }
    if (db) {
        db.collection('counts').count(function (err, count) {
            res.send('{ pageCount: ' + count + '}');
        });
    } else {
        res.send('{ pageCount: -1 }');
    }
});

// error handling
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something bad happened!');
});

initDb(function (err) {
    console.log('Error connecting to Mongo. Message:\n' + err);
});

app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

module.exports = app;
